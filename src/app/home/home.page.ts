import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { LocalStorageService } from '../app/core/services/local-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  usuario: string;
  password: string;
  constructor(private router: Router, private lsService: LocalStorageService) { }
  login() {
    console.log("Usuario: ", this.usuario);
    console.log("Pw: ", this.password);
    let obj = {
      nombre: this.usuario,
      pw: this.password
    }
    this.lsService.saveItem("info", obj);
    console.log(this.lsService.getItem("info"));
    this.router.navigate(['/dash']);
  }
}
