import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  saveItem(name, obj){
    localStorage.setItem(name,JSON.stringify(obj));
  }
  getItem(name){
    return localStorage.getItem(name);
  }
}
