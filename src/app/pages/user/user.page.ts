import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
  providers: [
    BarcodeScanner
  ]
})
export class UserPage implements OnInit {
  value = "https://ionicacademy.com/";
  scannedCode = null;
  elementType: 'url' | 'canvas' | 'img' = 'url';
  changePw: boolean = false;
  users = [
    { nombre: 'Juan', apellido: 'Barreiro' },
    { nombre: 'Mariano', apellido: 'Tribuj' },
    { nombre: 'Marcelo', apellido: 'Lopez' },
    { nombre: 'Pablo', apellido: 'Mega' },
    { nombre: 'Mariano', apellido: 'Zawadzki' }
  ];
  constructor(private barcodeScanner: BarcodeScanner) { }

  ngOnInit() {
  }

  showModif() {
    this.changePw = !this.changePw;
  }
  cambiarPw() {
    console.log("Cambiar PW");
  }
  nuevoContacto(nombre){
    console.log("Nuevo contacto con: ", nombre);
  }
}
